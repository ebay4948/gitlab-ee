export default {
  sast: {
    isLoading: false,
    hasError: false,
    newIssues: [],
    resolvedIssues: [],
    allIssues: [],
  },
  sastContainer: {
    approved: [],
    unapproved: [],
    vulnerabilities: [],
  },
  dast: [],
  codeclimate: {
    newIssues: [],
    resolvedIssues: [],
  },
  dependencyScanning: {
    isLoading: false,
    hasError: false,
    newIssues: [],
    resolvedIssues: [],
    allIssues: [],
  },
};
